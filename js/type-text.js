(function($, Drupal, drupalSettings) {
	"use strict";
	Drupal.behaviors.typeText = {
		attach: function(context, settings) {
		var typed = new Typed('.video-content-description', {
		stringsElement: "#container-video-description",
		typeSpeed: 200,
		backDelay: 2000,
        startDelay: 100,
		backSpeed: 40,
		loop: true
		});
	}	
  };
})(jQuery, Drupal, drupalSettings);