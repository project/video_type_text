README.txt for Video Type Text module
---------------------------
  
CONTENTS OF THIS FILE
---------------------
  
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
  
INTRODUCTION
------------

Video Type Text module to have a new content type Video Type Text and if
the video content is added to it then it will have video content description
in typed sentence.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 - Install the User Instance module as you would normally install a 
 contributed Drupal module. Visit https://www.drupal.org/node/1897420
 for further information.


CONFIGURATION
-------------

1. Place this module in your /modules directory and enable it.
2. Go to add content and select Video Type Text and give the description
and video.
3. As content is added the you can see the video with description in
typing format.
4. If content type is not required then you can delete the content type
and use the similar structure followed in node--video-type-text.html


MAINTAINERS
-----------

 - Naveen Kumar N (naveen-k) https://www.drupal.org/u/naveen-k

 - Supporting organizations
   Moonraft Innovation Labs
