<?php

/**
 * @file
 * Install, update, and uninstall functions for the Video Type Text module.
 */

/**
 * Implements hook_uninstall() for video_type_text_uninstall().
 */
function video_type_text_uninstall() {

  $result = \Drupal::entityQuery("node")
    ->condition("type", "video_type_text")
    // If the update is being executed via drush entityQuery will only
    // return the content uid 0 have access to. To return all set
    // accessCheck to false since it defaults to TRUE.
    ->accessCheck(FALSE)
    ->execute();

  if (!empty($result)) {
    $storage_handler = \Drupal::entityTypeManager()->getStorage("node");
    $entities = $storage_handler->loadMultiple($result);
    $storage_handler->delete($entities);
  }
}
